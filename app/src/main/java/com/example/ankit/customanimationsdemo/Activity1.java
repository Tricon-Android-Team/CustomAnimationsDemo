package com.example.ankit.customanimationsdemo;

import android.os.Build;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.transition.Explode;
import android.transition.Fade;
import android.transition.Slide;
import android.view.Gravity;
import android.view.Window;

public class Activity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inside your activity (if you did not enable transitions in your theme)
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            // set an exit transition
            //getWindow().setExitTransition(new Explode());
            //getWindow().setExitTransition(new Fade());
            //getWindow().setExitTransition(new Slide(Gravity.LEFT));

            // set an enter transition
            //getWindow().setEnterTransition(new Explode());
            //getWindow().setEnterTransition(new Fade());
            //getWindow().setEnterTransition(new Slide(Gravity.LEFT));
        }
    }
}

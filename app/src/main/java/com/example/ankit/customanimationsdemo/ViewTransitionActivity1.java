package com.example.ankit.customanimationsdemo;

import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.transition.AutoTransition;
import android.view.Window;
import android.widget.ImageView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class ViewTransitionActivity1 extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inside your activity (if you did not enable transitions in your theme)
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_view_transition1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            getWindow().setSharedElementEnterTransition(new AutoTransition());
        }

        ImageView ivScene = (ImageView) findViewById(R.id.iv_scene1);

        if (ivScene != null) {
            Picasso.with(this)
                    .load(R.drawable.scene1)
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            int targetWidth, targetHeight;
                            double aspectRatio;

                            if (source.getWidth() > source.getHeight()) {
                                targetWidth = 600;
                                aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                                targetHeight = (int) (targetWidth * aspectRatio);
                            } else {
                                targetHeight = 600;
                                aspectRatio = (double) source.getWidth() / (double) source.getHeight();
                                targetWidth = (int) (targetHeight * aspectRatio);
                            }

                            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                            if (result != source) {
                                source.recycle();
                            }
                            return result;
                        }

                        @Override
                        public String key() {
                            return "myKey";
                        }
                    })
                    .into(ivScene);
        }
    }
}

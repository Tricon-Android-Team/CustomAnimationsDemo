package com.example.ankit.customanimationsdemo;

import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.app.ActivityOptions;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityOptionsCompat;
import android.support.v7.app.AppCompatActivity;
import android.transition.Slide;
import android.util.Pair;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

public class MainActivity extends AppCompatActivity {

    private TextView mTvReveal;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // inside your activity (if you did not enable transitions in your theme)
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);

        setContentView(R.layout.activity_main);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            getWindow().setAllowEnterTransitionOverlap(true);

            //TODO: comment activity transitions while showing view transitions

            // set an exit transition
            //getWindow().setExitTransition(new Explode());
            //getWindow().setExitTransition(new Fade());
            getWindow().setExitTransition(new Slide());

            // set an enter transition
            //getWindow().setEnterTransition(new Explode());
            //getWindow().setEnterTransition(new Fade());
            //getWindow().setEnterTransition(new Slide());
        }

        mTvReveal = (TextView) findViewById(R.id.tv_reveal);

        if (mTvReveal != null) {
            mTvReveal.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    hideView();
                }
            });
        }

        final ImageView ivScene1 = (ImageView) findViewById(R.id.iv_scene1);

        if (ivScene1 != null) {
            Picasso.with(this)
                    .load(R.drawable.scene1)
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            int targetWidth, targetHeight;
                            double aspectRatio;

                            if (source.getWidth() > source.getHeight()) {
                                targetWidth = 600;
                                aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                                targetHeight = (int) (targetWidth * aspectRatio);
                            } else {
                                targetHeight = 600;
                                aspectRatio = (double) source.getWidth() / (double) source.getHeight();
                                targetWidth = (int) (targetHeight * aspectRatio);
                            }

                            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                            if (result != source) {
                                source.recycle();
                            }
                            return result;
                        }

                        @Override
                        public String key() {
                            return "myKey";
                        }
                    })
                    .into(ivScene1);

            ivScene1.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    ActivityOptionsCompat options = ActivityOptionsCompat
                            .makeSceneTransitionAnimation(MainActivity.this, ivScene1, "scene1");
                    startActivity(new Intent(MainActivity.this, ViewTransitionActivity1.class), options.toBundle());

                }
            });
        }


        //For multiple view transitions
        final ImageView ivScene2 = (ImageView) findViewById(R.id.iv_scene2);

        if (ivScene2 != null) {
            Picasso.with(this)
                    .load(R.drawable.scene2)
                    .transform(new Transformation() {
                        @Override
                        public Bitmap transform(Bitmap source) {
                            int targetWidth, targetHeight;
                            double aspectRatio;

                            if (source.getWidth() > source.getHeight()) {
                                targetWidth = 600;
                                aspectRatio = (double) source.getHeight() / (double) source.getWidth();
                                targetHeight = (int) (targetWidth * aspectRatio);
                            } else {
                                targetHeight = 600;
                                aspectRatio = (double) source.getWidth() / (double) source.getHeight();
                                targetWidth = (int) (targetHeight * aspectRatio);
                            }

                            Bitmap result = Bitmap.createScaledBitmap(source, targetWidth, targetHeight, false);
                            if (result != source) {
                                source.recycle();
                            }
                            return result;
                        }

                        @Override
                        public String key() {
                            return "myKey";
                        }
                    })
                    .into(ivScene2);

            ivScene2.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    //NOTE: we can use ActivityOptionsCompat here
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                        ActivityOptions options = ActivityOptions
                                .makeSceneTransitionAnimation(MainActivity.this,
                                        Pair.create((View)ivScene2, "scene2"),
                                        Pair.create(findViewById(R.id.tv_scene2), "tv_scene2"));
                        startActivity(new Intent(MainActivity.this, ViewTransitionActivity2.class), options.toBundle());
                    }

                }
            });
        }
    }

    // previously visible view
    private void hideView() {

        // get the center for the clipping circle
        int cx = mTvReveal.getWidth() / 2;
        int cy = mTvReveal.getHeight() / 2;

        // get the initial radius for the clipping circle
        float initialRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the final radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mTvReveal, cx, cy, initialRadius, 0);
        } else {
            mTvReveal.setVisibility(View.INVISIBLE);

            //show after some time
            try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            showView();
        }

        if (anim != null) {
            // make the view invisible when the animation is done
            anim.addListener(new AnimatorListenerAdapter() {
                @Override
                public void onAnimationEnd(Animator animation) {
                    super.onAnimationEnd(animation);
                    mTvReveal.setVisibility(View.INVISIBLE);

                    //show after some time
                    try {
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                    showView();
                }
            });
            anim.start();
        }
    }

    // previously invisible view
    private void showView() {

        // get the center for the clipping circle
        int cx = mTvReveal.getWidth() / 2;
        int cy = mTvReveal.getHeight() / 2;

        // get the final radius for the clipping circle
        float finalRadius = (float) Math.hypot(cx, cy);

        // create the animator for this view (the start radius is zero)
        Animator anim = null;
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
            anim = ViewAnimationUtils.createCircularReveal(mTvReveal, cx, cy, 0, finalRadius);
        }

        // make the view visible and start the animation
        mTvReveal.setVisibility(View.VISIBLE);
        if (anim != null) {
            anim.start();
        }
    }

    public void startNewActivity(View v) {
        switch (v.getId()) {
            case R.id.btn_explode:
                startActivity(new Intent(this, Activity1.class),
                        ActivityOptionsCompat.makeSceneTransitionAnimation(this).toBundle());
                break;
        }
    }

}
